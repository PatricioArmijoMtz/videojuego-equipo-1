﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class BombController : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D other) {
    	if (other.gameObject.CompareTag("Fighter")) {
    		other.gameObject.GetComponent<Movimiento>().TakeHit(transform, 25);
    		Destroy(gameObject);
    	}
    }

}
