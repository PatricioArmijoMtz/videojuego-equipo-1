﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour {

    private Rigidbody2D rb;
    private Animator animator;
    private CharacterControl characterControl;
    public GameController gameController;
    [SerializeField] private GameObject bombPrefab;

    public float movimientoHorizontal;
    private bool isOnGround = false;
    private bool isFalling = false;
    public bool doJump = false;
    private bool canJump = true;
    public int currentAttack = -1;
    private bool isTakingHit = false;
    private bool lookingAtRight = true;
    public bool canTakeLaser = true;
    public int playerID;

    public GameObject blood;

    [SerializeField] private float speed = 5.0f;
    [SerializeField] private float jumpForce = 250.0f;
    [SerializeField] private float fallingThreshold = 1.0f;
    [SerializeField] private int[] attacksDamage = {5, 10};
    [SerializeField] private string[] attacksSounds;
    [SerializeField] private float attackVolume = 1.0f;
    [SerializeField] private float jumpPitch = 1.0f;
    [SerializeField] private float jumpVolume = 1.0f;
    [SerializeField] private float hurtPitch = 1.0f;
    [SerializeField] private float hurtVolume = 1.0f;
    [SerializeField] private float defaultHealth = 100;
    private float health;

    public Image fillImage;

    private void Start() {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        characterControl = GetComponent<CharacterControl>();
        health = defaultHealth;
    }

    private void Update() {
        if (movimientoHorizontal > 0) {
            if (!lookingAtRight) {
                TurnAround();
            }
            animator.SetBool("isRunning", true);
        } else if (movimientoHorizontal < 0) {
            if (lookingAtRight) {
                TurnAround();
            }
            animator.SetBool("isRunning", true);
        } else {
            animator.SetBool("isRunning", false);
        }
    }

    private void FixedUpdate() {
        rb.velocity = new Vector2(movimientoHorizontal * speed, rb.velocity.y);
        if (doJump) {
            Jump();
        }
        if (rb.velocity.y < -fallingThreshold) {
            Fall();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        GameObject other = collision.gameObject;
        Movimiento otherMovimiento = other.GetComponent<Movimiento>();
        if (isFalling && (other.CompareTag("Ground")) || other.CompareTag("Fighter") && other.transform.position.y < transform.position.y && transform.position.x < other.transform.position.x+1f && transform.position.x > other.transform.position.x-1f) {
            Idle();
        } else if (other.CompareTag("Fighter") && currentAttack >= 0 && !otherMovimiento.isTakingHit) {
            if (lookingAtOpponent(collision.transform)) {
                otherMovimiento.TakeHit(transform, attacksDamage[currentAttack]);
            }
        }
    }

    private void OnColliderEnter2D(Collider2D other) {
        if (isFalling && other.CompareTag("Ground")) {
            Idle();
        }
    }

    public void Attack(int attackNo) {
        if (currentAttack == -1) {
            animator.SetTrigger("attack" + attackNo);
            currentAttack = attackNo;
            gameController.AddSource(AudioManager.instance.Play(attacksSounds[currentAttack], attackVolume, 1, false), 0, playerID);
        }
    }

    public void Die() {
        animator.SetBool("isDead", true);
        currentAttack = -1;
        movimientoHorizontal = 0.0f;
        characterControl.enabled = false;
        gameObject.tag = "Dead";
        gameController.GameOver(gameObject);
        AudioManager.instance.Play("Death", 1f, 1, false);
    }

    private bool lookingAtOpponent(Transform opponent) {
        return (lookingAtRight == (transform.position.x < opponent.position.x));
    }

    public void RestartLaserTaking() {
        StartCoroutine("RestartLaserRoutine");
    }

    private IEnumerator RestartLaserRoutine() {
        canTakeLaser = false;
        yield return new WaitForSeconds(1.5f);
        canTakeLaser = true;
    }

    public void TakeHit(Transform opponent, int damage) {
        if (!lookingAtOpponent(opponent)) {
            TurnAround();
        }
        if (health <= damage) {
            Die();
        } else {
            animator.SetTrigger("takeHit");
            isTakingHit = true;
        }
        health -= damage;
        gameController.AddSource(AudioManager.instance.Play("Hurt", hurtVolume, hurtPitch, false), 1, playerID);
        fillImage.fillAmount = health / defaultHealth;
        spawnBlood();
    }

    private void AttackOver() {
        currentAttack = -1;
    }

    private void spawnBlood() {
        Vector3 position = transform.position;
        position.z = position.z - 1;
        Instantiate(blood, position, Quaternion.identity);
    }

    private void TakeHitOver() {
        isTakingHit = false;
    }

    private void Jump() {
        doJump = false;
        if (isOnGround && canJump) {
            animator.SetBool("isJumping", true);
            rb.AddForce(transform.up * jumpForce);
            isOnGround = false;
            StartCoroutine("JumpReload");
            gameController.AddSource(AudioManager.instance.Play("Jump", jumpVolume, jumpPitch, false), 2, playerID);
        }
    }

    private IEnumerator JumpReload() {
        canJump = false;
        yield return new WaitForSeconds(1);
        canJump = true;
    }
    
    private void Fall() {
        isFalling = true;
        animator.SetBool("isFalling", true);
        animator.SetBool("isJumping", false);
    }

    private void Idle() {
        animator.SetBool("isFalling", false);
        isFalling = false;
        isOnGround = true;
    }

    public void TurnAround() {
        int direction = lookingAtRight ? -1 : 1;
        lookingAtRight = !lookingAtRight;
        transform.localScale = new Vector2(direction * Mathf.Abs(transform.localScale.x), transform.localScale.y);
    }

}