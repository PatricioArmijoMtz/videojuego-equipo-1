﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound {

	public string name;
	public AudioClip clip;

	[HideInInspector]
	public float volume;
	[HideInInspector]
	public float pitch;
	[HideInInspector]
	public bool loop;
	[HideInInspector]
	public AudioSource source;
}