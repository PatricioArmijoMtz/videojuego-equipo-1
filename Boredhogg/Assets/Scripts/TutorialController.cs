﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour{

	[SerializeField] private GameObject tutorial1;
	[SerializeField] private GameObject tutorial2;

	public void ShowTutorial2() {
		print("show tutorial 2");
		tutorial1.SetActive(false);
		tutorial2.SetActive(true);
	}

	public void ShowTutorial1() {
		print("show tutorial 1");
		tutorial2.SetActive(false);
		tutorial1.SetActive(true);
	}

}
