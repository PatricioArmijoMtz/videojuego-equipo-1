﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectionController : MonoBehaviour {
	[SerializeField] private GameObject[] prefabs;
	[SerializeField] private Image[] playerBGs;
	private bool[] characterSelected = new bool[2];
	private List<GameObject>[] instantiations = new List<GameObject>[2];
	private int[] currentCharacters = new int[2];
	private int maxIndex;

	private void Start() {
		characterSelected[1] = false;
		maxIndex = prefabs.Length - 1;
		instantiations[0] = new List<GameObject>();
		instantiations[1] = new List<GameObject>();
		foreach (GameObject prefab in prefabs) {
			GameObject instantiation1 = Instantiate(prefab, new Vector2(-4.8f, 0.0f), Quaternion.identity);				
			GameObject instantiation2 = Instantiate(prefab, new Vector2(4.8f, 0.0f), Quaternion.identity);				
			instantiation1.SetActive(false);
			instantiation2.SetActive(false);
			instantiations[0].Add(instantiation1);
			instantiations[1].Add(instantiation2);
		}
		instantiations[0][0].SetActive(true);
		instantiations[1][0].SetActive(true);
		currentCharacters[0] = 0;
		currentCharacters[1] = 0;
	}

	private void Update() {
		if (!characterSelected[0]) {
			if (Input.GetKeyDown(KeyCode.A)) {
				SwitchTo(maxIndex, 0);
			} else if (Input.GetKeyDown(KeyCode.D)) {
				SwitchTo(1, 0);
			} else if (Input.GetKeyDown(KeyCode.Tab)) {
				SelectCharacter(0);
			}
		}
		if (!characterSelected[1]) {
			if (Input.GetKeyDown(KeyCode.LeftArrow)) {
				SwitchTo(maxIndex, 1);
			} else if (Input.GetKeyDown(KeyCode.RightArrow)) {
				SwitchTo(1, 1);
			} else if (Input.GetKeyDown(KeyCode.Return)) {
				SelectCharacter(1);
			}
		}
	}

	private void SwitchTo(int step, int playerID) {
		instantiations[playerID][currentCharacters[playerID]].SetActive(false);
		currentCharacters[playerID] = (currentCharacters[playerID] + step) % prefabs.Length;
		instantiations[playerID][currentCharacters[playerID]].SetActive(true);
	}

	private void SelectCharacter(int playerID) {
		characterSelected[playerID] = true;
		playerBGs[playerID].GetComponent<Image>().color = new Color32(88,202,54,255);
		PlayerData.playerCharacters[playerID] = currentCharacters[playerID];
		if (characterSelected[1-playerID]) {
			SceneManager.LoadScene("SampleScene");
		}
	}

}
