﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundManager : MonoBehaviour {

    public SpriteRenderer spriteRenderer;
    public Sprite Ronda1;
    public Sprite Ronda2;
    public Sprite Ronda3;
    private static int numRonda;

    // Update is called once per frame
    void Update() {
        numRonda = GameController.round;
        if (numRonda == 1)
        {
            spriteRenderer.sprite = Ronda1;
        }
        if (numRonda == 2)
        {
            spriteRenderer.sprite = Ronda2;
        }
        if (numRonda == 3)
        {
            spriteRenderer.sprite = Ronda3;
        }

    }
}