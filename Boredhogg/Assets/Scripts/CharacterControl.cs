﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour {

	private Movimiento movimiento;
	public string playerInputCode;

	private void Start() {
		movimiento = GetComponent<Movimiento>();
	}

    private void Update() {
        movimiento.movimientoHorizontal = Input.GetAxisRaw("Horizontal" + playerInputCode);
        if (Input.GetButton("Jump" + playerInputCode)) {
            movimiento.doJump = true;
        }
        if (Input.GetButton("Attack0" + playerInputCode)) {
            movimiento.Attack(0);
        }
        if (Input.GetButton("Attack1" + playerInputCode)) {
            movimiento.Attack(1);
        }
    }

}
