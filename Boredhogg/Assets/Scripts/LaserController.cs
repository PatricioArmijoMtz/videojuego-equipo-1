﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour {

	[SerializeField] private float distance = 200.0f;
	[SerializeField] private Transform laserFirePoint;
	[SerializeField] private LineRenderer m_lineRenderer;
	private Transform m_transform;
	private bool isActivated = false;

	private void Awake() {
		m_transform = GetComponent<Transform>();
	}

	public void ActivateLaser() {
		isActivated = true;
	}

	private void Update() {
		if (isActivated)
			ShootLaser();
	}

	void ShootLaser() {
		if (Physics2D.Raycast(m_transform.position, -transform.up)) {
			RaycastHit2D _hit = Physics2D.Raycast(laserFirePoint.position, -transform.up);
			Draw2DRay(laserFirePoint.position, _hit.point);
			if (_hit.transform.CompareTag("Fighter")) {
				Movimiento movimiento = _hit.transform.GetComponent<Movimiento>();
				if (movimiento.canTakeLaser) {
					movimiento.TakeHit(transform, 5);
					movimiento.RestartLaserTaking();
				}
			}
		} else {
			Draw2DRay(laserFirePoint.position, -laserFirePoint.transform.up * distance);
		}
	}

	private void Draw2DRay(Vector2 startPos, Vector2 endPos) {
		m_lineRenderer.SetPosition(0, startPos);
		m_lineRenderer.SetPosition(1, endPos);
	}
}
