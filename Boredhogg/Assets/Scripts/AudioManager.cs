﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public Sound[] sounds;
	public static AudioManager instance;

	private void Awake() {
		if (instance != null) {
			print("destroying instance");
			Destroy(instance.gameObject);
		}
		instance = this;

		// if (instance == null) {
			// instance = this;
		// } else {
			// Destroy(gameObject);
		// }

		DontDestroyOnLoad(gameObject);

		foreach(Sound sound in sounds) {
			sound.source = gameObject.AddComponent<AudioSource>();
			sound.source.clip = sound.clip;
		}
	}

	public AudioSource Play(string name, float volume, float pitch, bool loop) {
		Sound s = Array.Find(sounds, sound => sound.name == name);
		if (s == null) {
			Debug.LogWarning("No se encontró el sonido.");
			return null;
		}
		s.source.volume = volume;
		s.source.pitch = pitch;
		s.source.loop = loop;
		s.source.Play();
		return s.source;
	}        
}
