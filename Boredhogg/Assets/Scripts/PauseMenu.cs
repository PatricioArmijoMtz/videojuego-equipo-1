﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	[SerializeField] private GameObject pauseMenuCanvas;
	private GameController gameController;

	private bool gameIsPaused = false;
	private bool gameCanBePaused = true;

	private void Start() {
		gameController = GetComponent<GameController>();
		pauseMenuCanvas.SetActive(false);
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.Escape) && gameCanBePaused) {
			if (gameIsPaused) {
				ResumeGame();
			} else {
				PauseGame();
			}
		}
	}

	public void ResumeGame() {
		gameIsPaused = false;
		pauseMenuCanvas.SetActive(false);
		Time.timeScale = 1.0f;
		gameController.ResumeGameplay();
	}

	private void PauseGame() {
		gameIsPaused = true;
		pauseMenuCanvas.SetActive(true);
		Time.timeScale = 0.0f;
		gameController.PauseGameplay();
	}

    public void GoToMenu() {
    	Time.timeScale = 1.0f;
        SceneManager.LoadScene("MenuPrincipal");
    }

    public void MakeGamePausable() {
    	gameCanBePaused = true;
    }

    public void MakeGameUnpausable() {
    	gameCanBePaused = false;
    }

}
