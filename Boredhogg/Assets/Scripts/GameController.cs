﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using Pathfinding;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour {

    [SerializeField] private GameObject[] chPrefabs;
   	[SerializeField] private GameObject miniPlatformPrefab;
   	[SerializeField] private GameObject bombPrefab;
   	[SerializeField] private GameObject laserPrefab;
    [SerializeField] private Image healthFill1;
    [SerializeField] private Image healthFill2;
    [SerializeField] private TextMeshProUGUI winnerText;
    [SerializeField] private float musicVolume = 1.0f;

    private GameObject player1;
    private GameObject player2;
    private CharacterControl player1Control;
    private CharacterControl player2Control;
    private IEnumerator bombsRoutine;
    private List<GameObject> bombs;
    private GameObject laserSphere;
    private IEnumerator laserRoutine;
    private bool sphereMovingDown = false;

    private AudioSource gameplayMusic;
    private AudioSource[,] audioSources = new AudioSource[3, 2];
    //private AudioSource[] attackSources = new AudioSource[2];
    //public AudioSource[] defendSources;
    //public AudioSource[] jumpSources;
    private PauseMenu pauseMenuController;

    [SerializeField] private float restartTime = 3.0f;
    [SerializeField] private float returnToMenu = 5.0f;

    private void Start() {
        gameplayMusic = AudioManager.instance.Play("GameplayMusic", musicVolume, 1, true);
        pauseMenuController = GetComponent<PauseMenu>();
        Instantiate(miniPlatformPrefab, new Vector2(-5.5f, -.5f), Quaternion.identity);
        Instantiate(miniPlatformPrefab, new Vector2(5.5f, -.5f), Quaternion.identity);
        NewGame();
    }

    private void Update() {
    	if (sphereMovingDown) {
    		if (laserSphere.transform.position.y < 4f) {
    			sphereMovingDown = false;
    			StartCoroutine("ActivateLaser");
    		}
    		laserSphere.transform.position += -Vector3.up * .75f * Time.deltaTime;
    	}
    }

    private IEnumerator ActivateLaser() {
    	yield return new WaitForSeconds(3);
    	laserSphere.transform.GetChild(0).GetComponent<LaserController>().ActivateLaser();
    }

    private IEnumerator GenerateBombs() {
    	yield return new WaitForSeconds(Random.Range(5, 30));
    	int target = Random.Range(0, 1);
    	Follow(target == 0 ? player1.transform : player2.transform, new Color(1, 0, 0, 1));
    	yield return new WaitForSeconds(Random.Range(5, 30));
    	Follow(target == 0 ? player2.transform : player1.transform, new Color(0, 1, 0, 1));
    	bombsRoutine = GenerateBombs();
    	StartCoroutine(bombsRoutine);
    }

    private void PlayMusic() {
        gameplayMusic.Play();
    }

    private void PauseMusic() {
        gameplayMusic.Pause();
    }

    public void AddSource(AudioSource src, int type, int playerID) {
        audioSources[type, playerID] = src;
    }

    public void PauseGameplay() {
        PauseMusic();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                if (audioSources[i, j] != null) {
                    if (audioSources[i, j].isPlaying) {
                        audioSources[i, j].Pause();
                    } else {
                        audioSources[i, j] = null;
                    }
                }
            }
        }
        player1Control.enabled = false;
        player2Control.enabled = false;
    }

    public void ResumeGameplay() {
        PlayMusic();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                if (audioSources[i, j] != null) {
                    audioSources[i, j].Play();
                }
            }
        }
        player1Control.enabled = true;
        player2Control.enabled = true;
    }

    public static int round = 1;

    private void NewGame() {
		pauseMenuController.MakeGamePausable();
        bombs = new List<GameObject>();
		winnerText.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		healthFill1.fillAmount = 1.0f;
		healthFill2.fillAmount = 1.0f;
		player1 = Instantiate(chPrefabs[PlayerData.playerCharacters[0]], new Vector2(-5.0f, -2.5f), Quaternion.identity);
		player2 = Instantiate(chPrefabs[PlayerData.playerCharacters[1]], new Vector2(5.0f, -2.5f), Quaternion.identity);
		player2.GetComponent<Movimiento>().TurnAround();
		player1Control = player1.GetComponent<CharacterControl>();
		player2Control = player2.GetComponent<CharacterControl>();
		Movimiento player1Movimiento = player1.GetComponent<Movimiento>();
		Movimiento player2Movimiento = player2.GetComponent<Movimiento>();
        player1Movimiento.canTakeLaser = true;
        player2Movimiento.canTakeLaser = true;
		player1Movimiento.playerID = 0;
		player2Movimiento.playerID = 1;
		player1Control.playerInputCode = "";
		player2Control.playerInputCode = "2";
		player1Movimiento.gameController = this;
		player2Movimiento.gameController = this;
		player1Movimiento.fillImage = healthFill1;
		player2Movimiento.fillImage = healthFill2;
		if (round < 3) {
        	bombsRoutine = GenerateBombs();
        	StartCoroutine(bombsRoutine);
		} else if (round == 3) {
			laserRoutine = GenerateLaser();
			StartCoroutine(laserRoutine);
		}
    }

    private IEnumerator GenerateLaser() {
    	//yield return new WaitForSeconds(Random.Range(5, 25));
    	yield return new WaitForSeconds(1);
    	laserSphere = Instantiate(laserPrefab, new Vector2(Random.Range(-6, 6), 7.5f), Quaternion.identity);
    	sphereMovingDown = true;
    }

    public void Follow(Transform followed, Color color) {
        GameObject bomb = Instantiate(bombPrefab, new Vector2(0, 4), Quaternion.identity);
        bombs.Add(bomb);
        bomb.GetComponent<AIDestinationSetter>().target = followed;
        bomb.transform.GetChild(0).GetComponent<SpriteRenderer>().color = color;
    }

    private void backChanger()
    {
        throw new NotImplementedException();
    }

    public void GameOver(GameObject deadPlayer) {
        player1.GetComponent<Movimiento>().canTakeLaser = false;
        player2.GetComponent<Movimiento>().canTakeLaser = false;
        foreach (GameObject bomb in bombs) {
            Destroy(bomb);
        }
		if (deadPlayer == player1) {
			winnerText.text = "¡Jugador 2 gana!";
		} else if (deadPlayer == player2) {
			winnerText.text = "¡Jugador 1 gana!";
		}
		pauseMenuController.MakeGameUnpausable();
		winnerText.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        if (round < 3)
        {
            StartCoroutine("RestartGame");
            StopCoroutine(bombsRoutine);
        }
        else
        {
            StartCoroutine("CargarMenu");
        }
	}

	private IEnumerator RestartGame() {
		yield return new WaitForSeconds(restartTime);
		Destroy(player1);
		Destroy(player2);
        round = round + 1;
        NewGame();
	}

    private IEnumerator CargarMenu()
    {
        yield return new WaitForSeconds(returnToMenu);
        round = 1;
        SceneManager.LoadScene("MenuPrincipal");
    }
}
