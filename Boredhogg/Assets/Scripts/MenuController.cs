﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

	[SerializeField] private float musicVolume = 1.0f;

	private void Start() {
		AudioManager.instance.Play("MenuMusic", musicVolume, 1, true);
	}

}
